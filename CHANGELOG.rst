Version 6.1.2 (2024-02-10)
=====================================================

* Explicitly set nodeLinker config to fix compatibility with Yarn 4.x 

Version 6.1.1 (2024-02-09)
=====================================================

* Fix compatibility with Yarn 4.x
* Drop usage of six

Version 6.1.0 (2023-02-19)
=====================================================

* Pass on yarn's exit code

Version 6.0.3 (2022-11-01)
=====================================================

* Fix compatibility with Django 4.1
* Pass all arguments of yarn commands to yarn
* [Dev] Fix tests

Version 6.0.2  (2022-09-05)
=====================================================

* Fix yarnpkg call for non-install commands

Version 6.0.1  (2020-01-03)
=====================================================

* Prefer the yarnpkg command over yarn if installed due to naming conflict
  on Debian

Version 6.0  (2019-12-07)
=====================================================

* Fork and port to yarnpkg

Version 4.8  (2013-10-02)
=====================================================

* Support of bower arguments in bower_install

Version 4.7 (2013-09-01)
======================================================

* Bower 1.2 support.
* Better code coverage.

Version 4.6 (2013-08-11)
======================================================

* Add bower 1.1 support.
* Add django 1.6 support.
* Remove dev dependencies from setup.py.

Version 4.5 (2013-07-30)
======================================================

* Remove bower < 1.0 support.

Version 4.4 (2013-07-30)
======================================================

* Add bower >= 1.0 support.
* Fix freeze packages from git.

Version 4.3 (2013-07-16)
======================================================

* Add bower 0.10 support.
* Add python 3.3 support.
